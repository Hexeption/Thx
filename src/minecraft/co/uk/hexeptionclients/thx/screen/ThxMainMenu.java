/*
 * Copyright � 2016 | Hexeption | All rights reserved.
 *
 */
package co.uk.hexeptionclients.thx.screen;

import java.awt.Color;
import java.io.IOException;

import co.uk.hexeptionclients.thx.Thx;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiMainMenu;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.ResourceLocation;

public class ThxMainMenu extends GuiMainMenu {
	
	private static final ResourceLocation minecraftTitleTextures = new ResourceLocation("textures/gui/title/minecraft.png");
	
	public ThxMainMenu() {
		super();
	}
	
	@Override
	protected void actionPerformed(GuiButton button) throws IOException {
		super.actionPerformed(button);
		
		switch (button.id) {
		case 100:
//			TODO: Alt Login
//			mc.displayGuiScreen();
			break;
		case 101:
//			TODO: Client Options
//			mc.displayGuiScreen();
			break;
		}
	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		GlStateManager.disableAlpha();
		renderSkybox(mouseX, mouseY, partialTicks);
		GlStateManager.enableAlpha();

		drawGradientRect(0, 0, this.width, this.height, -2130706433, 16777215);
		drawGradientRect(0, 0, this.width, this.height, 0, Integer.MIN_VALUE);

		int i = 274;
		int j = this.width / 2 - i / 2;
		int k = 30;
		mc.getTextureManager().bindTexture(minecraftTitleTextures);
		GlStateManager.color(1.0f, 1.0f, 1.0f);
		drawTexturedModalRect(j + 0, k + 0, 0, 0, 155, 44);
		drawTexturedModalRect(j + 155, k + 0, 0, 45, 155, 44);

		String MinecraftVersion = "Minecraft 1.10";
		String MinecraftCopyright = "Copyright Mojang AB";
		String MinecraftCopyright2 = "Do not distribute!";

		String OpiatesVersion = Thx.clientName + " " + Thx.clientVersion;
		String OpiatesCopyright = "Copyright '" + Thx.clientCreator + "'";
		String OpiatesCopyright2 = "All right reserved";

		drawString(fontRendererObj, MinecraftVersion, width - fontRendererObj.getStringWidth(MinecraftVersion) - 5, this.height - 29, Color.white.getRGB());
		drawString(fontRendererObj, MinecraftCopyright, width - fontRendererObj.getStringWidth(MinecraftCopyright) - 5, this.height - 19, Color.white.getRGB());
		drawString(fontRendererObj, MinecraftCopyright2, width - fontRendererObj.getStringWidth(MinecraftCopyright2) - 5, this.height - 9, Color.white.getRGB());

		drawString(fontRendererObj, OpiatesVersion, 5, this.height - 29, Color.white.getRGB());
		drawString(fontRendererObj, OpiatesCopyright, 5, this.height - 19, Color.white.getRGB());
		drawString(fontRendererObj, OpiatesCopyright2, 5, this.height - 9, Color.white.getRGB());
		
		for(Object button : buttonList){
			((GuiButton) button).drawButton(mc, mouseX, mouseY);
		}
	}

}
