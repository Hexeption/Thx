/*
 * Copyright � 2016 | Hexeption | All rights reserved.
 *
 */
package co.uk.hexeptionclients.thx.event.events.update;

import com.darkmagician6.eventapi.events.Event;

public class EventPostMotionUpdates implements Event
{
    public float	yaw;
    public float	pitch;
    public double	y;
    public boolean	ground;
    public boolean	motionUpdatesRan;
    public float	motionUpdatesPitch;
    public float	motionUpdatesYaw;

    public void UpdateEvent()
    {
    }

    public void UpdateEvent(double y, float yaw, float pitch, boolean ground)
    {
        this.yaw = yaw;
        this.pitch = pitch;
        this.y = y;
        this.ground = ground;
    }
}
