/*
 * Copyright � 2016 | Hexeption | All rights reserved.
 *
 */
package co.uk.hexeptionclients.thx.event.events.render;

import com.darkmagician6.eventapi.events.Event;

public class EventRender2D implements Event
{
    public int width;
    public int height;

    public EventRender2D(final int width, final int height)
    {
        this.width = width;
        this.width = height;
    }
}
