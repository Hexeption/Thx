package co.uk.hexeptionclients.thx.event.events.render;

import com.darkmagician6.eventapi.events.Event;

public class EventRenderChat implements Event
{
    private String text;

    public EventRenderChat(String text)
    {
        this.text = text;
    }

    public String getString()
    {
        return text;
    }
}
