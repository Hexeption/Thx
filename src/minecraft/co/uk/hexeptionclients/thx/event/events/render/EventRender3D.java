/*
 * Copyright � 2016 | Hexeption | All rights reserved.
 *
 */
package co.uk.hexeptionclients.thx.event.events.render;

import com.darkmagician6.eventapi.events.Event;

public class EventRender3D implements Event
{
    public float	partiaTicks;

    public EventRender3D(float partiaTicks)
    {
        this.partiaTicks = partiaTicks;
    }
}
