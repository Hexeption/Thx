/*
 * Copyright � 2016 | Hexeption | All rights reserved.
 *
 */
package co.uk.hexeptionclients.thx.event.events.network;

import com.darkmagician6.eventapi.events.callables.EventCancellable;

import net.minecraft.network.Packet;

public class EventPacketRecieve extends EventCancellable
{
    private boolean	cancel;
    private Packet	packet;

    public EventPacketRecieve(Packet packet)
    {
        this.packet = packet;
    }

    public boolean isCancel()
    {
        return cancel;
    }

    public void setCancel(boolean cancel)
    {
        this.cancel = cancel;
    }

    public Packet getPacket()
    {
        return packet;
    }
}
