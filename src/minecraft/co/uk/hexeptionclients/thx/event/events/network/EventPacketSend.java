/*
 * Copyright � 2016 | Hexeption | All rights reserved.
 *
 */
package co.uk.hexeptionclients.thx.event.events.network;

import com.darkmagician6.eventapi.events.callables.EventCancellable;

import net.minecraft.network.Packet;

public class EventPacketSend extends EventCancellable
{
    public Packet	packet;

    public EventPacketSend(Packet p)
    {
        this.packet = p;
    }

    public Packet getPacket()
    {
        return packet;
    }

    private boolean	canceled	= true;

    public boolean getCanceled()
    {
        return this.canceled;
    }

    public void setCanceled(boolean canceled)
    {
        this.canceled = canceled;
    }
}
