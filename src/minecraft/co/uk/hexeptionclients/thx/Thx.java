/*
 * Copyright � 2016 | Hexeption | All rights reserved.
 *
 */
package co.uk.hexeptionclients.thx;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import co.uk.hexeptionclients.thx.ui.Hud;

public class Thx {
	
	public static String clientName = "Thx";
	public static String clientVersion = "1 Build";
	public static String clientCreator = "Hexeption";
	
	public static Logger logger = LogManager.getLogger();
	
	private static Hud hud = new Hud();
	
	public void startClient(){
		logger.debug("Loading " + clientName);
		logger.debug("Created by " + clientCreator);
		
		this.hud.loadThemes();
		
		logger.debug("Finished loading " + clientName);
		
	}

}
