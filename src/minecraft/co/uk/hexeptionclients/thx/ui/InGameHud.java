/*
 * Copyright � 2016 | Hexeption | All rights reserved.
 *
 */
package co.uk.hexeptionclients.thx.ui;

import net.minecraft.client.Minecraft;

public interface InGameHud {
	
	public abstract void render(Minecraft minecraft, int displayWidht, int displayHeight);
	
	public abstract String getName();
	
	public abstract void onKeyPressed(int key);

}
