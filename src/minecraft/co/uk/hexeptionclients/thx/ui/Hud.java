/*
 * Copyright � 2016 | Hexeption | All rights reserved.
 *
 */
package co.uk.hexeptionclients.thx.ui;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import com.darkmagician6.eventapi.EventManager;
import com.darkmagician6.eventapi.EventTarget;

import co.uk.hexeptionclients.thx.core.Wrapper;
import co.uk.hexeptionclients.thx.event.events.render.EventRender2D;
import co.uk.hexeptionclients.thx.event.events.update.EventKeyboard;
import co.uk.hexeptionclients.thx.ui.themes.ThxTheme;

public class Hud {

	private final List<InGameHud>	themes		= new CopyOnWriteArrayList();
	private int						themeIndex	= 0;
	
	public Hud() {
		EventManager.register(this);
	}
	
	public void loadThemes(){
		themes.add(new ThxTheme());
	}
	
	@EventTarget
	public void render(EventRender2D event){
		if(Wrapper.getInstance().getGameSettings().showDebugInfo){
			return;
		}
		
		InGameHud currentTheme = getCurrentTheme();
		currentTheme.render(Wrapper.getInstance().getMinecraft(), Wrapper.getInstance().getDisplayWidth(), Wrapper.getInstance().getDisplayHeight());
	}
	
	public InGameHud getCurrentTheme(){
		return (InGameHud) this.themes.get(this.themeIndex);
	}
	
	public void onTextTheme(){
		int index = this.themeIndex;
		int maxSize = this.themes.size();
		
		if(index != 1){
			index++;
			
			if(index >= maxSize){
				index = 0;
			}
			
			this.themeIndex = index;
		}
	}
	
	public InGameHud getThme(String name){
		for(InGameHud theme : this.themes){
			if(theme.getName().equals(name)){
				return theme;
			}
		}
		return null;
	}
	
	@EventTarget
	public void onKeyEvent(EventKeyboard event){
		getCurrentTheme().onKeyPressed(event.key);
	}

}
