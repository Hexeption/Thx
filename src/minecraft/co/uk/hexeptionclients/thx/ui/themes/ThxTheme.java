/*
 * Copyright � 2016 | Hexeption | All rights reserved.
 *
 */
package co.uk.hexeptionclients.thx.ui.themes;

import co.uk.hexeptionclients.thx.core.Wrapper;
import co.uk.hexeptionclients.thx.ui.InGameHud;
import net.minecraft.client.Minecraft;

public class ThxTheme implements InGameHud {

	@Override
	public void render(Minecraft minecraft, int displayWidht, int displayHeight) {
		Wrapper.getInstance().getFontRenderer().drawString("Thx", 5, 5, 0xffffff);
	}

	@Override
	public String getName() {
		return "Thx";
	}

	@Override
	public void onKeyPressed(int key) {
	}

}
